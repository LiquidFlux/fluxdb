'use strict';

angular.module('fluxdbApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/db', {
        templateUrl: 'app/db/db.html',
        controller: 'DbCtrl'
      });
  });
