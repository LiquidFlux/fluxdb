'use strict';

angular.module('fluxdbApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'Satellizer',
  'ngAnimate',
  'monospaced.elastic',
  'angularFileUpload'
])
  .config(function ($routeProvider, $locationProvider, $authProvider) {

    $authProvider.logoutRedirect = '/';
    $authProvider.loginRedirect = '/db';
    $authProvider.signupRedirect = '/';
    $authProvider.loginUrl = '/api/me/login';
    $authProvider.signupUrl = '/api/me/signup';
    $authProvider.loginRoute = '/login';
    $authProvider.signupRoute = '/signup';
    $authProvider.user = 'currentUser';
    $authProvider.google({
      clientId: '886700724899-l1u02olfuebkot670mf9vihhtdl99oa0.apps.googleusercontent.com'
    });

    $routeProvider
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider.html5Mode(true);
  });