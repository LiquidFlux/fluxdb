'use strict';

angular.module('fluxdbApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/db/create', {
        templateUrl: 'app/create/create.html',
        controller: 'CreateCtrl'
      });
  });
