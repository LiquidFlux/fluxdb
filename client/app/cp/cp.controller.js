'use strict';

angular.module('fluxdbApp')
  .controller('CPCtrl', function ($scope) {
    $scope.cp = [];

    $http.get('/api/cp/').success(function(data) {
      $scope.cp = data;
    });
  });
