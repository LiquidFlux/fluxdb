'use strict';

angular.module('fluxdbApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/cp', {
        templateUrl: 'app/cp/cp.html',
        controller: 'MainCtrl'
      });
  });
