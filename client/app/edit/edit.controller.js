'use strict';

angular.module('fluxdbApp')
  .controller('EditCtrl', function ($scope) {
    $scope.mission = [];

    $http.get('/api/db/').success(function(data) {
      $scope.mission = data;
    });
  });
