'use strict';

angular.module('fluxdbApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/db/:query/edit', {
        templateUrl: 'app/edit/edit.html',
        controller: 'MainCtrl'
      });
  });
