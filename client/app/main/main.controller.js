'use strict';

angular.module('fluxdbApp')
  .controller('MainCtrl', function ($scope, $http, $routeParams, $auth, Account, $location, $upload) {
  

    Account.getProfile()
      .success(function(data){
        $scope.currentUser = data;

        // CP User Missions --
        $scope.userMissions = [];
        
        $http.get('/api/db/user/' + $scope.currentUser._id).success(function(data) {
          $.each( data, function( key, value ){
            if( value.status == 1 || value.status == 2 ){
              $scope.userMissions.push(value);
            }
          });
        });

      })
      .error(function(){
      })

    // Auth --
    $scope.authenticate = function(provider){
      $auth.authenticate(provider);
    };

    $scope.login = function() {
      $auth.login({ 
        email: $scope.email, 
        password: $scope.password
      }).then(function(response) {
      })
      .catch(function(response) {
      });
    };

    $scope.test = function() {
      console.log($scope.currentUser);
    };

    $scope.authenticate = function(provider) {
      $auth.authenticate(provider)
        .then(function() {
          //success
        })
        .catch(function(response) {
          //failure
        });
    };

    $scope.logout = function(){
      $auth.logout();
    }

    $scope.signup = function() {
      // create account
      $auth.signup({
        displayName: $scope.displayName,
        email: $scope.email,
        password: $scope.password
      }).then(function(){
        $http.get('/api/me').success(function(data) {
          $scope.currentUser = data;
          console.log(data);
        });
      });
    };

    $scope.updateProfile = function() {
      var profileData = {
        displayName: $scope.currentUser.displayName,
        password: $scope.currentUser.password
      };

      Account.updateProfile(profileData).then(function() {
      })
    };

    



    // DB --
    $scope.DB = [];

    $http.get('/api/db').success(function(data) {

      $.each( data, function( key, value ){
        if( value.status == 1 || value.status == 2 ){
          $scope.DB.push(value);
        }
      });


    });

    $scope.advancedFilters = [];
    $scope.advancedFilters.statusList = [1, 2];
    $scope.advancedFilters.status = [1];

    $scope.toggleSelection = function(filter){
      var idx = $scope.advancedFilters.status.indexOf(filter);

      if( idx > -1 ){
        $scope.advancedFilters.status.splice(idx, 1);
        console.log($scope.advancedFilters.status);
      } else {
        $scope.advancedFilters.status.push(filter);
        console.log($scope.advancedFilters.statusSelection);
      }
    }

    $scope.showMission = function(mission){
      $location.path('/db/' + mission._id);
    };



    // Mission --

    $scope.mission = [];

    if($routeParams.query){
      $http.get('/api/db/' + $routeParams.query).success(function(data) {

        $scope.mission = data;

        // These need populating for editing and updating missions
        $scope.nameModel = $scope.mission.name;
        $scope.descriptionModel = $scope.mission.description;
        $scope.playerCountMinModel = $scope.mission.playerCountMin;
        $scope.playerCountMaxModel = $scope.mission.playerCountMax;

        // Begins loops over the objects to match current definitions against the arrays
        // Platform
        $.each( $scope.platforms, function( key, value ){
          if( $scope.mission.platform == value.name ){
            $scope.platformModel = value;
          }
        });

        // Type
        // we use '.toLowerCase' here due to capitalising types on the frontend and not the backend
        $.each( $scope.types, function( key, value ){
          if( $scope.mission.type == value.name.toLowerCase() ){
            $scope.typeModel = value;
          }
        });

        // Terrain
        $.each( $scope.terrains, function( key, value ){
          if( $scope.mission.terrain == value.name ){
            $scope.terrainModel = value;
          }
        });

      }).then( function(){
        // success
        // Used for 'Other missions by this user'
        $scope.otherMissions = [];

        $http.get('/api/db/user/' + $scope.mission.authorID).success(function(data) {

          $.each( data, function( key, value ){
            if( $scope.mission._id != value._id ){
              if( value.status == 1 ){
                $scope.otherMissions.push(value);
              }
            }
          });
        });
      }, function(){
        // failure
      });
    }

    $scope.editMission = function(mission){
      $location.path('/db/' + mission._id + '/edit');
    }


    // Create --

    $scope.create = [];

    $scope.terrains = [
      { id: 1, name: 'Chernarus'},
      { id: 2, name: 'Takistan'},
      { id: 3, name: 'Sahrini'},
      { id: 4, name: 'Altis'}
    ];

    $scope.platforms = [
      { id: 1, name: 'A3'},
      { id: 2, name: 'A2'}
    ];

    $scope.types = [
      { id: 1, name: 'Co-op'},
      { id: 2, name: 'Adv'},
      { id: 2, name: 'Training'}
    ];

    $scope.createMission = function(){

      var postData = {
        name: $scope.missionForm.name.$modelValue,
        authorID: $scope.currentUser._id || '',
        author: $scope.currentUser.displayName || '',
        type: $scope.missionForm.type.$modelValue.name.toLowerCase(),
        description: $scope.missionForm.description.$modelValue,
        terrain: $scope.missionForm.terrain.$modelValue.name,
        platform: $scope.missionForm.platform.$modelValue.name,
        playerCountMin: $scope.missionForm.playerCountMin.$modelValue,
        playerCountMax: $scope.missionForm.playerCountMax.$modelValue,
        updates: [{
          reason: 'Mission created',
          date: Date.now()
        }],
        status: 1
      };


      $http.post('/api/db', postData).then(function(data) {
        $location.path('/db/' + data.data._id);
      });
    };

    $scope.onFileSelect = function($files){
      console.log('$scope.onFileSelect');
      //$files: an array of files selected, each file has name, size, and type.
      for ( var i = 0; i < $files.length; i++ ){
        var $file = $files[i];
        $upload.upload({
          url: 'my/upload/url',
          file: $file,
          progress: function(e){}
        }).then( function(data, status, headers, config){
          // file is uploaded successfully
        });
      }
    }

    // Edit --

    $scope.updateMission = function(){

      var postData = {
        name: $scope.missionForm.name.$modelValue,
        type: $scope.missionForm.type.$modelValue.name.toLowerCase(),
        description: $scope.missionForm.description.$modelValue,
        terrain: $scope.missionForm.terrain.$modelValue.name,
        platform: $scope.missionForm.platform.$modelValue.name,
        playerCountMin: $scope.missionForm.playerCountMin.$modelValue,
        playerCountMax: $scope.missionForm.playerCountMax.$modelValue,
        updates: {
          reason: $scope.missionForm.updateNotes.$modelValue,
          date: Date.now()
        }
      };

      $http.put('/api/db/' + $scope.mission._id, postData).then(function(data) {
        $location.path('/db/' + data.data._id);
      });
    };

    // Delete mission

    $scope.confirmDeleteMission = function(mission){

      var postData = {
        status: 3
      };

      $http.put('/api/db/' + mission._id, postData).then(function(data) {
        console.log('------------------ STATUS - 3 ------------------');
        console.log(data);
        console.log('------------------ STATUS - 3 ------------------');
        $location.path('/db');
      });
    }



  })
  .factory('Account', function($http, $auth) {
    return {
      getProfile: function() {
        return $http.get('/api/me');
      },
      updateProfile: function(profileData) {
        return $http.put('/api/me', profileData).success(function(data) {
          $auth.updateToken(data.token);
        });
      }
    };
  });