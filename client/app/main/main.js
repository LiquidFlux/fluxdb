'use strict';

angular.module('fluxdbApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      });
    $routeProvider
      .when('/db', {
        templateUrl: 'app/db/db.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/db'
      });
    $routeProvider
      .when('/db/create', {
        templateUrl: 'app/create/create.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/db'
      });
    $routeProvider
      .when('/db/:query', {
        templateUrl: 'app/main/mission.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/db'
      });
  });