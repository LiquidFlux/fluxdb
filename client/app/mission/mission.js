'use strict';

angular.module('fluxdbApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/db/:query', {
        templateUrl: 'app/mission/mission.html',
        controller: 'MainCtrl'
      });
  });
