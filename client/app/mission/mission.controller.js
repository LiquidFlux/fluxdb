'use strict';

angular.module('fluxdbApp')
  .controller('MissionCtrl', function ($scope) {
    $scope.mission = [];

    $http.get('/api/db/').success(function(data) {
      $scope.mission = data;
    });
  });
