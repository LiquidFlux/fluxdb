'use strict';

angular.module('fluxdbApp')
  .controller('NavbarCtrl', function ($scope, $location) {
    $scope.menu = [{
      'title': 'DB',
      'link': '/db'
    },{
      'title': 'Create',
      'link': '/db/create'
    }];
    $scope.menu2 = [{
      'title': 'My Missions ',
      'link': '/cp'
    }];

    $scope.isCollapsed = true;

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });