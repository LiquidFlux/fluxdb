/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /DBs              ->  index
 * POST    /DBs              ->  create
 * GET     /DBs/:id          ->  show
 * PUT     /DBs/:id          ->  update
 * DELETE  /DBs/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var DB = require('./db.model');

// Get list of DBs
exports.index = function(req, res) {
  DB.find(function (err, DBs) {
    if(err) { return handleError(res, err); }
    return res.json(200, DBs);
  });
};

// Get a single DB by mission ID
exports.show = function(req, res) {
  DB.findById(req.params.id, function (err, DB) {
    if(err) { return handleError(res, err); }
    if(!DB) { return res.send(404); }
    return res.json(DB);
  });
};

// Get a single DB by user ID
exports.showByUserID = function(req, res) {
  console.log(req.params);
  DB.find({ authorID: req.params.id }, function (err, DB) {
    if(err) { return handleError(res, err); }
    if(!DB) { return res.send(404); }
    return res.json(DB);
  });
};

// Creates a new DB in the DB.
exports.create = function(req, res) {
  DB.create(req.body, function(err, DB) {
    if(err) { return handleError(res, err); }
    return res.json(201, DB);
  });
};

// Updates an existing DB in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  DB.findById(req.params.id, function (err, DB) {

    if (err) { 
      return handleError(res, err); 
    }

    if(!DB) { 
      return res.send(404); 
    }

    var bodyWithoutUpdates = _.omit(req.body,'updates');
    var updates = _.pick(req.body,'updates');
    var updated = _.merge(DB, bodyWithoutUpdates);

    DB.updates.push(updates.updates);

    updated.save(function (err) {
      if (err) { 
        return handleError(res, err); 
      }
      return res.json(200, DB);
    });
  });
};

// Deletes a DB from the DB.
exports.destroy = function(req, res) {
  DB.findById(req.params.id, function (err, DB) {
    if(err) { return handleError(res, err); }
    if(!DB) { return res.send(404); }
    DB.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}