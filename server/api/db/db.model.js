'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var updateSchema = new Schema({
  date: { type: Date, default: Date.now },
  reason: String
});

var DBSchema = new Schema({
  name: String,
  authorID: String,
  author: String,
  description: String,
  platform: String,
  type: String,
  terrain: String,
  playerCountMin: Number,
  playerCountMax: Number,
  status: Number,
  dateCreated: { type: Date, default: Date.now },
  updates: [updateSchema]
});


module.exports = mongoose.model('DB', DBSchema);