'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DBSchema = new Schema({
  name: String,
  description: String,
  platform: String,
  type: String,
  terrain: String,
  playerCountMin: Number,
  playerCountMax: Number
});

module.exports = mongoose.model('DB', DBSchema);