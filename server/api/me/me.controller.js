/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /DBs              ->  index
 * POST    /DBs              ->  create
 * GET     /DBs/:id          ->  show
 * PUT     /DBs/:id          ->  update
 * DELETE  /DBs/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var DB = require('./me.model');
var bcrypt = require('bcryptjs');
var moment = require('moment');
var jwt = require('jwt-simple');
var config = require('../../config/environment');


exports.createToken = function(req, user) {
  var payload = {

    iss: 'http://localhost:9000/',
    // iss: req.hostname,
    sub: user._id,
    iat: moment().valueOf(),
    exp: moment().add(14, 'days').valueOf()
  };
  console.log('createToken');
  console.log(payload);
  return jwt.encode(payload, config.TOKEN_SECRET);
}

exports.ensureAuthenticated = function(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send({ message: 'Please make sure your request has an Authorization header' });
  }

  var token = req.headers.authorization.split(' ')[1];
  var payload = jwt.decode(token, config.TOKEN_SECRET);

  if (payload.exp <= Date.now()) {
    return res.status(401).send({ message: 'Token has expired' });
  }
  req.user = payload.sub;
  next();
}

exports.get = function(req, res){
  DB.findById(req.user, function (err, user) {
    res.send(user);
  });
};

exports.update = function(req, res){
  DB.findById(req.user, function(err, user) {
    if (!user) {
      return res.status(400).send({ message: 'User not found' });
    }
    user.displayName = req.body.displayName || user.displayName;
    user.email = req.body.email || user.email;
    user.save(function(err) {
      res.send({ token: exports.createToken(req, user) });
    });
  });
};

exports.login = function(req, res){
  console.log('LOGIN');
  console.log(req.body);
  DB.findOne({ email: req.body.email }, '+password', function(err, user) {
    console.log('user found');
    console.log(user);
    if (!user) {
      console.log('could not find user by email');
      return res.status(401).send({ message: 'Wrong email and/or password' });
    }

    user.comparePassword(req.body.password, function(err, isMatch) {
      if (!isMatch) {
        console.log('passwords did not match');
        return res.status(401).send({ message: 'Wrong email and/or password' });
      } else {
        console.log('passwords match');
      }
      res.send({ 
        token: exports.createToken(req, user)
      });
      console.log('token should have been made and sent');
    });
  });
};

exports.signup = function(req, res){
  console.log('---///// signup function hit');
  console.log(DB);
  var user = new DB();
  user.displayName = req.body.displayName;
  user.email = req.body.email;
  user.password = req.body.password;
  user.save(function(err) {
    res.status(200).end();
  });
};

function handleError(res, err) {
  return res.send(500, err);
}