'use strict';

var express = require('express');
var controller = require('./me.controller');

var router = express.Router();



router.get('/', controller.ensureAuthenticated, controller.get);
router.put('/', controller.ensureAuthenticated, controller.update);
router.post('/login', controller.login);
router.post('/signup', controller.signup);


module.exports = router;