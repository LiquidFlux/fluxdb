/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var DB = require('../api/db/db.model');


DB.find({}).remove(function() {
  DB.create({
    name : 'Remove Potato',
    description: '',
    platform : 'A3',
    type: "adv",
    terrain: "Chernarus",
    playerCountMin: 20,
    playerCountMax: 300
  }, {
    name : 'Copping Out',
    description: '',
    platform : 'A3',
    type: "adv",
    terrain: "Stratis",
    playerCountMin: 24,
    playerCountMax: 82
  }, {
    name : 'Tigerland',
    description: '',
    platform : 'A3',
    type: "adv",
    terrain: "Chernarus",
    playerCountMin: 12,
    playerCountMax: 158
  },  {
    name : 'The Game',
    description: '',
    platform : 'A3',
    type: "adv",
    terrain: "South Sahrini",
    playerCountMin: 8,
    playerCountMax: 40
  },  {
    name : 'Liberators',
    description: '',
    platform : 'A3',
    type: "adv",
    terrain: "Takistan",
    playerCountMin: 32,
    playerCountMax: 187
  },{
    name : 'Get Lost',
    description: '',
    platform : 'A3',
    type: "training",
    terrain: "Altis",
    playerCountMin: 1,
    playerCountMax: 21
  });
});